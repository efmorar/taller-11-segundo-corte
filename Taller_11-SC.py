#upper
print("Buenos diastardes o noches, en este programa aprenderás a usar Upper. Upper es una cadena de texto usada simplemente para mantener una cadena en mayuscula. El método .upper() convetirá la cadena de texto ingresada en una cadena de texto toda en mayuscula, intentemoslo:")
print("En este caso usaremos la cadena, X")
cadena="X"
print(cadena.upper())
print("Inserta un valor o letras:")
cadena2=input("Para iniciar, ingresa una cadena de texto=")
print("Acabas de crear una cadena de texto TODA en minúscula, ahora imprimirás la cadena ingresada y en su contenido usarás cadena.upper(), para realizar lo manifestado.")
print(cadena2.upper())
print("Listo, nuevo codigo enlazado a tu memoria (.upper()) con las cadenas de texto")
#lower
print("Holi, en este programa aprenderás a usar otra cadena de texto, la cual es Lower. Lower es una cadena de texto usada simplemente para mantener una cadena en minúscula. El método .lower() convetirá la cadena de texto ingresada en una cadena de texto toda en minúscula, Hagamoslo:")
print("Un ejemplo de la cadena seria, CadenaLower")
cadena3="CadenaLower"
print(cadena3.lower())
print("Dale tu bebe:")
cadena4=input("Para comenzar, ingresa una cadena de texto=")
print("Acabas de crear una cadena de texto TODA en mayúscula, ahora imprimirás la cadena ingresada y en su contenido usarás cadena.lower(), para realizar lo ingresado.")
print(cadena4.lower())
print("Gracias por ver, nueva cadena aprendida (.lower()) con las cadenas de texto")
#strip
print("Holi de nuevo, asi es, otro tipo de cadena de texto, en este caso será Strip. Strip es una cadena de texto usada simplemente para quitarnos espaciados que no queremos ver impresos.Puede ser:")
print("Un ejemplo seria,     Y   ")
cadena5="   Y    "
print(cadena5.strip())
print("Dale tu:")
cadena6=input("Para comenzar, ingresa una cadena de texto=")
print("Bien!, creaste una cadena de texto a la que le quitarás espaciados, que darás con la tecla Tab, ahora imprimirás la cadena ingresada y en su contenido usarás cadena.strip(), para realizar lo digitado.")
print(cadena6.strip())
print("Fin, nuevo codigo aprendido (.strip()) con las cadenas de texto")
#startswith
print("Helou, ya has visto antes ciertas cadenas, esta no sera la exepcion, en este caso será startswith. Startswith es una cadena de texto usada simplemente para comprobación de inicio de cadenas.Osea:")
print("Para este ejemplo usaremos,AND")
cadena7="AND"
print(cadena7.startswith("A"))
print("Come on, do it:")
cadena8=input("Bienvenido, ingresa una cadena de texto=")
print("Excelente, hiciste una cadena de texto en la que comprobarás su inicio, ahora imprimirás la cadena ingresada y en su contenido usarás cadena.startswith() dentro del parentesis lo que comprobarás, para hacer lo dicho.")
print(cadena8.startswith(""))
print("Hasta aca llega el tutorial de StartSwitch, nueva cadena aprendida (.startswith()) con las cadenas de texto")
#endswith
print("Hola de nuevo, asi es, otra cadena de texto, en este caso será endswith. Endswith es una cadena de texto usada simplemente para comprobación de final de cadenas.Como por ejemplo:")
print("Para este caso usaremos la cadena,AND")
cadena9="AND"
print(cadena9.endswith("ND"))
print("Tu turno:")
cadena10=input("Comienza ingresando una cadena de texto=")
print("Genial,creaste una cadena de texto en la que comprobarás su final, ahora imprimirás la cadena ingresada y en su contenido usarás cadena.endswith() dentro del parentesis lo que comprobarás, para realizar lo escrito.")
print(cadena10.endswith(""))
print("This is the end, Prosigue con el siguiente tutorial(.endswith()) con las cadenas de texto")
#find
print("Ya sabes, otro tutorial de cadena de texto, en este caso será find. Find es una cadena de texto usada simplemente para buscar un dato en la cadena.Ejemplo:")
print("La cadena sera,Hola Mundo")
cadena11="Hola Mundo"
print(cadena11.find("Mundo"))
print("Intentalo:")
cadena12=input("Para iniciar, ingresa una cadena de texto=")
print("Asombroso, acabas de crear una cadena de texto en la que buscarás un dato, ahora imprimirás la cadena ingresada y en su contenido usarás cadena.find() dentro del parentesis lo que comprobarás, para realizar lo acordado.")
print(cadena12.find(""))
print("Conclusion, aprendiste a usar un nuevo codigo (.find()) con las cadenas de texto")
#replace
print("Sisi, Hola, otro programa de cadena de texto, en este caso será replace. Replace es una cadena de texto usada simplemente para reemplazar un dato en la cadena.Como puede ser:")
print("Hoy usaremos, Extraño a mi Ex")
cadena13="Extraño a mi ex"
print(cadena13.replace("ex", "EX:("))
print("Facil, dale tu:")
cadena14=input("Para comenzar, ingresa una cadena de texto=")
print("Gucci, hiciste una cadena de texto en la que reemplazarás un dato, ahora imprimirás la cadena ingresada y en su contenido usarás cadena.replace() dentro del parentesis lo que cambiarás, para realizar lo digitado.")
print(cadena14.replace(""))
print("Genial, aprendiste un nuevo codigo (.replace()) con las cadenas de texto")
#split
print("Ya este es mi ultimo saludo, vamos a aprender la ultima cadena de texto de este tutorial, en este caso será split. Split es una cadena de texto usada simplemente para romper una cadena.Ajam:")
print("Para esta cadena sera, Supreme Original")
cadena15="Supreme Original"
print(cadena15.split("  "))
print("Ya sabes como es:")
cadena14=input("Comienza ingresando una cadena de texto=")
print("Esplendido, ya casi eres un experto creando cadenas de texto en la que romperas sus datos, ahora imprimirás la cadena ingresada y en su contenido usarás cadena.split() dentro del parentesis lo que cambiarás, para realizar lo manifestado.")
print(cadena14.split(""))
print("Con esto concluimos el tutorial, suerte con lo que quieras programar (.split()) con las cadenas de texto")
